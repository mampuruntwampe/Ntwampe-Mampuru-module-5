import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'abstract_widgets/primary_textfield.dart';
import 'models/note_model.dart';

class NoteView extends StatefulWidget {
  const NoteView({Key? key}) : super(key: key);

  @override
  _NoteViewState createState() => _NoteViewState();
}

class _NoteViewState extends State<NoteView> {
  final titleController = TextEditingController();
  final messageController = TextEditingController();
  final dateController = TextEditingController();
  final db = FirebaseFirestore.instance;
  dynamic noteList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Notes",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold))),
      body: StreamBuilder(
        stream: db.collection('notes').snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data?.docs.length,
            itemBuilder: (context, int index) {
              noteList = snapshot.data.docs;
              return ListTile(
                onTap: () async {

                  showDialog(context: context, builder:(BuildContext context){
                    return Dialog(
                      alignment: Alignment.center,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const SizedBox(
                                height: 10,
                              ),
                              const Text("Update Note",style: TextStyle(fontSize:30)),
                              const SizedBox(
                                height: 30,
                              ),
                              TextfieldWidget(label: "${noteList[index]["title"]}",controller: titleController,textInputType: TextInputType.text,),
                              const SizedBox(
                                height: 20,
                              ),
                              TextfieldWidget(label: "${noteList[index]["message"]}",controller: messageController,textInputType: TextInputType.text,),
                              const SizedBox(
                                height: 20,
                              ),
                              TextfieldWidget(label: "${noteList[index]["date"]}",controller: dateController,textInputType: TextInputType.text,),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  TextButton(
                                    style: ButtonStyle(
                                      foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: const Text('Cancel',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                                  ),

                                  TextButton(
                                    style: ButtonStyle(
                                      foregroundColor: MaterialStateProperty.all<Color>(Colors.green),
                                    ),
                                    onPressed: () async {
                                      updateNote(index);
                                      Navigator.pop(context);
                                    },
                                    child: const Text('Create',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  });
                },
                leading: const Icon(
                  Icons.assignment_sharp,
                  color: Colors.teal,
                ),
                title: Text(noteList[index]['title']),
                subtitle: Text(
                    "${noteList[index]['message']}   ${noteList[index]['date']}"),
                trailing: IconButton(
                  icon: const Icon(Icons.delete_outline, color: Colors.red),
                  onPressed: () async {
                     await deleteNote(noteList[index]['id']);
                  },
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.create),
        onPressed: () {

          showDialog(context: context, builder:(BuildContext context){
            return Dialog(
              alignment: Alignment.center,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Add a Note",style: TextStyle(fontSize:30)),
                      const SizedBox(
                        height: 30,
                      ),
                      TextfieldWidget(label: "Title",controller: titleController,textInputType: TextInputType.text,),
                      const SizedBox(
                        height: 20,
                      ),
                      TextfieldWidget(label: "Message",controller: messageController,textInputType: TextInputType.text,),
                      const SizedBox(
                        height: 20,
                      ),
                      TextfieldWidget(label: "Date",controller: dateController,textInputType: TextInputType.text,),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextButton(
                            style: ButtonStyle(
                              foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text('Cancel',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                          ),

                          TextButton(
                            style: ButtonStyle(
                              foregroundColor: MaterialStateProperty.all<Color>(Colors.green),
                            ),
                            onPressed: () async {
                              createNote();
                              Navigator.pop(context);
                            },
                            child: const Text('Create',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          });
        },
      ),
    );
  }

  //Function to create a note in the Firestore
  Future<void>  createNote() async {
    final noteDB = FirebaseFirestore.instance.collection('notes').doc();

    final event = Note(
        id: noteDB.id,
        title: titleController.text,
        message: messageController.text,
        date: dateController.text);

    final value = event.toJson();

    await noteDB.set(value);
  }

  //Function that updates a note from firestore
  Future<void> updateNote(int index) async {
    final db = FirebaseFirestore.instance.collection("notes").doc(noteList[index]["id"]);

    String title;
    String message;
    String date;

    if(titleController.text != ''){
      title = titleController.text;
    }else{
      title = noteList[index]['title'];
    }

    if(messageController.text != ''){
      message = messageController.text;
    }else{
      message = noteList[index]['message'];
    }

    if(dateController.text != ''){
      date = dateController.text;
    }else{
      date = noteList[index]['date'];
    }

    await db.update({
      'title': title,
      'message': message,
      'date': date,
    }
    );
  }

  //Function that deletes a note from firestore
  Future<void> deleteNote(String id) async{
    db.collection('notes').doc(id).delete();
  }

}
